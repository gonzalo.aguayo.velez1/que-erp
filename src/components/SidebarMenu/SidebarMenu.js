import React, { Component } from 'react'
import { NavLink, withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Layout, Menu, Icon } from 'antd'

import './SidebarMenu.scss'

const { Sider } = Layout;

class SideBarMenu extends Component {
  constructor(props) {
    super(props)
    this.state = {
      theme: 'dark'
    }
  }

  static propTypes = {
    collapsed: PropTypes.bool,
    location: PropTypes.object.isRequired,
  }

  render() {
    const { location } = this.props;
    const items = [{
      icon: 'home',
      to: '/',
      text: 'Inicio'
    }, {
      icon: 'user',
      to: '/usuarios',
      text: 'Usuarios'
    }, {
      icon: 'layout',
      to: '/almacenes',
      text: 'Almacenes'
    }, {
      icon: 'appstore',
      to: '/productos',
      text: 'Productos'
    }, {
      icon: 'shopping',
      to: '/pos',
      text: 'Punto de Venta'
    }, {
      icon: 'dollar',
      to: '/monedas',
      text: 'Moneda'
    }]

    return (
      <Sider
        collapsible
        collapsed={this.props.collapsed}
      >
        <div className="logo" />
        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={['/']}
          selectedKeys={[location.pathname]}
        >
          {items.map(item =>
            <Menu.Item key={item.to}>
              <Icon type={item.icon} />
              <span>{item.text}</span>
              <NavLink to={item.to} />
            </Menu.Item>
          )}
        </Menu>
      </Sider>
    )
  }
}

export default withRouter(SideBarMenu);