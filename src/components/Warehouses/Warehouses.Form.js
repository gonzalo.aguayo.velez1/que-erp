import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Modal, Input } from 'antd';

import { formItemLayout } from 'utils/forms';

class WarehouseForm extends Component {
  static propTypes = {
    mode: PropTypes.string,
    visible: PropTypes.bool,
    onOk: PropTypes.func,
    onCancel: PropTypes.func,
  }

  static defaultProps = {
    record: {}
  }

  render() {
    const { visible, mode, onOk, onCancel, form } = this.props;
    const okText = mode === 'new' ? 'Guardar' : 'Actualizar';
    const title = mode === 'new' ? 'Crear Nuevo Almacen' : `Almacen ${this.props.record.username}`
    const { getFieldDecorator } = form;

    return (
      <Modal
        title={title}
        style={{ top: 20 }}
        visible={visible}
        okText={okText}
        onOk={onOk}
        onCancel={onCancel}
      >
        <Form {...formItemLayout}>
          <Form.Item hasFeedback label="Nombre">
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Ingrese el nombre del Almacen.' }],
            })(
              <Input />
            )}
          </Form.Item>

          <Form.Item label="Descripción">
            {getFieldDecorator('description')(<Input type="text" />)}
          </Form.Item>
        </Form>
      </Modal>
    )
  }
}

export default Form.create({ name: 'warehouse_form_modal' })(WarehouseForm);
