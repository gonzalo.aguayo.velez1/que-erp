import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Modal, Table } from 'antd';

import { getAllWarehousesWithBins } from 'services/warehouses';

export default class BinsSearch extends Component {
  constructor(props) {
    super(props);
    this.state = { bins: [], selectedRowKeys: [] }
  }

  static propTypes = {
    visible: PropTypes.bool,
    onCancel: PropTypes.func,
    onOk: PropTypes.func
  }

  loadBins = () => {
    const search = {};
    getAllWarehousesWithBins(search, ({ data = [] }) => {
      const bins = [];
      data.forEach(({ name, Bin = [] }) => {
        Bin.forEach(bin => {
          bins.push({ ...bin, warehouse: name })
        })
      });

      this.setState({ bins })
    });
  }

  onSelectChange = selectedRowKeys => {
    this.setState({ selectedRowKeys });
  }

  sendSelectedBins = () => {
    const { selectedRowKeys } = this.state;
    this.props.onOk(selectedRowKeys)
  }

  componentDidMount() {
    this.loadBins();
  }

  columns = [{
    title: 'Deposito',
    dataIndex: 'warehouse',
    key: 'warehouse',
  }, {
    title: 'Compartimiento',
    dataIndex: 'name',
    key: 'name',
  }, {
    title: 'Descripción',
    dataIndex: 'description',
    key: 'description',
  }];

  render() {
    const title = 'Buscar Compartimiento';
    const { visible, onOk, onCancel } = this.props;
    const { bins, selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;

    return (
      <Modal
        title={title}
        style={{ top: 20 }}
        visible={visible}
        okText={`Agregar ${selectedRowKeys.length}`}
        onOk={this.sendSelectedBins}
        onCancel={onCancel}
        okButtonDisabled={hasSelected}
      >
        <Table
          rowSelection={rowSelection}
          columns={this.columns}
          dataSource={bins}
          rowKey="_id"
        />
      </Modal>
    )
  }
}
