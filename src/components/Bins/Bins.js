import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { PageHeader, Tag, Layout, Button, message, Tooltip } from 'antd';

import 'components/Bins/Bins.style.scss';

import ComponentForm from 'components/Bins/Bins.Form';
import { getWarehouse } from 'services/warehouses';
import { getAllBins, createBin, updateBin } from 'services/bins';

class Bins extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      record: {},
      bins: [],
      showForm: false,
      mode: 'new',
      isLoading: false,
    };
  }

  static propTypes = {
  }

  showForm = (mode, record = {}) => {
    this.setState({ showForm: true, mode, record });

    this.form.resetFields();
    this.form.setFieldsValue(record);
  }

  handleOk = (e) => {
    const { record } = this.state;
    this.form.validateFields((err, newValues) => {
      if (err) {
        message.warning('Verifique la informacion en el formulario.');
        return;
      }

      const data = { ...record, ...newValues, warehouse: this.warehouse };
      this.form.resetFields();
      this.setState({ showForm: false });
      this.state.mode === 'new' ?
        createBin(data, this.loadRecord) :
        updateBin(data, this.loadRecord);
    });
  }

  handleCancel = (e) => {
    this.setState({ showForm: false });
  }

  handleDelete = (id) => {
    // deleteWarehouse(id, this.loadWarehouses);
  }

  loadRecord = () => {
    const { match: { params: { id } } } = this.props;
    this.warehouse = id;
    getWarehouse(id, data => this.setState({ data }));
    getAllBins(id, bins => this.setState({ bins }))
  }

  binComponent = (bin, index) => {
    return (
      <Tooltip
        placement="bottom"
        title={bin.description}
        arrowPointAtCenter
      >
        <div className="bin" key={index}>
          {bin.name}
        </div>
      </Tooltip>
    )
  }

  saveFormRef = (formRef) => {
    if (formRef) {
      this.form = formRef.props.form;
    }
  }

  componentDidMount() {
    this.loadRecord();
  }

  render() {
    const { data: { name, description, enable }, bins } = this.state;

    return (
      <Layout>
        <PageHeader
          onBack={() => window.history.back()}
          title={name}
          subTitle={description}
          tags={<Tag color={enable ? 'green' : 'red'}>{enable ? 'Activo' : 'Deshabilitado'}</Tag>}
          extra={[
            <Button
              key="1"
              icon="plus"
              type="primary"
              onClick={() => this.showForm('new')}
            >
              AGREGAR
            </Button>,
          ]}
        >
          <div className="wrap">
            {bins.map((bin, index) => this.binComponent(bin, index))}
          </div>
        </PageHeader>

        <ComponentForm
          wrappedComponentRef={this.saveFormRef}
          mode={this.state.mode}
          record={this.state.record}
          visible={this.state.showForm}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        />
      </Layout>
    )
  }
}

export default withRouter(Bins);