import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { Form, Modal, Input } from 'antd';

import { formItemLayout } from 'utils/forms';

class BinsForm extends Component {
  static propTypes = {
  }

  render() {
    const { visible, mode, onOk, onCancel, form } = this.props;
    const okText = mode === 'new' ? 'Guardar' : 'Actualizar';
    const title = mode === 'new' ? 'Crear Nuevo Compartimiento' : `Compartimiento ${this.props.record.name}`
    const { getFieldDecorator } = form;

    return (
      <Modal
        title={title}
        style={{ top: 20 }}
        visible={visible}
        okText={okText}
        onOk={onOk}
        onCancel={onCancel}
      >
        <Form {...formItemLayout}>
          <Form.Item hasFeedback label="Nombre">
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Ingrese el nombre del Compartimiento.' }],
            })(
              <Input />
            )}
          </Form.Item>

          <Form.Item label="Descripción">
            {getFieldDecorator('description')(<Input type="text" />)}
          </Form.Item>
        </Form>
      </Modal>
    )
  }
}

export default Form.create({ name: 'bin_form_modal' })(BinsForm);