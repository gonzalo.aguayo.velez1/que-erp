import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Divider, Form, Modal, Input, Radio } from 'antd';

import { formItemLayout } from 'utils/forms';

class UserForm extends Component {
  static propTypes = {
    mode: PropTypes.string,
    visible: PropTypes.bool,
    onOk: PropTypes.func,
    onCancel: PropTypes.func,
  }

  static defaultProps = {
    record: {}
  }

  render() {
    const { visible, mode, onOk, onCancel, form } = this.props;
    const okText = mode === 'new' ? 'Guardar' : 'Actualizar';
    const title = mode === 'new' ? 'Crear Nuevo Usuario' : `Usuario ${this.props.record.username}`
    const { getFieldDecorator } = form;

    return (
      <Modal
        title={title}
        style={{ top: 20 }}
        visible={visible}
        okText={okText}
        onOk={onOk}
        onCancel={onCancel}
      >
        <Form {...formItemLayout}>
          <Form.Item hasFeedback label="Usuario">
            {getFieldDecorator('username', {
              rules: [{ required: true, message: 'Ingrese el nombre de Usuario.' }],
            })(
              <Input />
            )}
          </Form.Item>

          <Form.Item label="Correo">
            {getFieldDecorator('email')(<Input type="email" />)}
          </Form.Item>

          <Form.Item label="Codigo Acceso">
            {getFieldDecorator('accessCode')(<Input />)}
          </Form.Item>

          <Form.Item label="Privilegio" className="collection-create-form_last-form-item">
            {getFieldDecorator('access', {
              initialValue: 'Administrador',
            })(
              <Radio.Group>
                <Radio value="Administrador">Administrador</Radio>
                <Radio value="Empleado">Empleado</Radio>
              </Radio.Group>
            )}
          </Form.Item>

          <Divider />

          <Form.Item label="Nombre(s)">
            {getFieldDecorator('firstName')(<Input />)}
          </Form.Item>

          <Form.Item label="Apellidos">
            {getFieldDecorator('lastName')(<Input />)}
          </Form.Item>

          <Form.Item label="Teléfono">
            {getFieldDecorator('phone')(<Input />)}
          </Form.Item>

          <Form.Item label="Direccion">
            {getFieldDecorator('address')(<Input />)}
          </Form.Item>
        </Form>
      </Modal>
    )
  }
}

export default Form.create({ name: 'user_form_modal' })(UserForm);
