import React, { Component } from 'react';
import { Button, Divider, Input, Icon, Form, Popconfirm, Table, Tag, Typography, Spin, message } from 'antd';

import UserForm from 'components/Users/Users.Form';
import { getAllUsers, deleteUser, createUser, updateUser } from 'services/users';

const { Text } = Typography;

export default class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      showForm: false,
      mode: 'new',
      record: {},
      isLoading: false,
    };
  }

  static propTypes = {
  }

  loadUsers = async (search = {}) => {
    try {
      this.setState({ isLoading: true });
      await getAllUsers(search, ({data, pagination}) => {
        this.setState({ data, pagination, isLoading: false })
      });
    } catch (error) {
      console.error(error);
      message.error('Hubo un problema al cargar los usuarios, por favor vuelta a intentarlo.');
      this.setState({ isLoading: false });
    }
  }

  showForm = (mode, record = {}) => {
    this.setState({ showForm: true, mode, record });
    this.form.resetFields();
    this.form.setFieldsValue(record);
  }

  handleOk = (e) => {
    const { record } = this.state;
    this.form.validateFields((err, newValues) => {
      if (err) {
        message.warning('Verifique la informacion en el formulario.');
        return;
      }

      const data = { ...record, ...newValues };
      this.form.resetFields();
      this.setState({ showForm: false });
      this.state.mode === 'new' ?
        createUser(data, this.loadUsers) :
        updateUser(data, this.loadUsers);
    });
  }

  handleCancel = (e) => {
    this.setState({ showForm: false });
  }

  handleDelete = (id) => {
    deleteUser(id, this.loadUsers);
  }

  handleSearch = (e) => {
    e.preventDefault();
    const search = document.getElementById('search-term').value;
    this.loadUsers({search});
  }

  handleTableChange = (pagination, filters, sorter) => {
    const search = document.getElementById('search-term').value;
    this.loadProducts({search, ...pagination});
  }

  saveFormRef = (formRef) => {
    if (formRef) {
      this.form = formRef.props.form;
    }
  }

  columns = [{
    title: 'Usuario',
    dataIndex: 'username',
    key: 'username'
  }, {
    title: 'Nombre',
    dataIndex: 'firstName',
    key: 'firstName',
  }, {
    title: 'Apellidos',
    dataIndex: 'lastName',
    key: 'lastName',
  }, {
    title: 'Email',
    dataIndex: 'email',
    key: 'email',
  }, {
    title: 'Teléfono',
    dataIndex: 'phone',
    key: 'phone',
  }, {
    title: 'Acceso',
    key: 'access',
    dataIndex: 'access',
    render: access => {
      const color = access === 'Administrador' ? 'geekblue' : 'green';
      return <span>
        <Tag color={color}>{access.toUpperCase()}</Tag>
      </span>
    },
  }, {
    title: 'Acciones',
    key: 'action',
    render: (text, record) => (
      <span className="table-actions">
        <Text onClick={() => this.showForm('edit', record)}>Editar</Text>
        <Divider type="vertical" />
        <Popconfirm
          placement="topRight"
          title={`Desea borrar al Usuario ${record.username}?`}
          onConfirm={() => this.handleDelete(record._id)}
          okText="Si"
          cancelText="No"
        >
          <Text type="danger">Borrar</Text>
        </Popconfirm>
      </span>
    ),
  }];

  componentDidMount() {
    this.loadUsers();
  }

  render() {
    return (
      <div className="form-grid">
        <Form layout="inline" onSubmit={this.handleSearch}>
          <Form.Item>
            <Input
              id="search-term"
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Buscar Usuario"
              style={{ width: 320 }}
              type="text"
            />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" icon="search">Buscar</Button>
          </Form.Item>
        </Form>
        <div className="actions">
          <Button
            icon="plus"
            type="primary"
            onClick={() => this.showForm('new')}
          >
            AGREGAR USUARIO
          </Button>
        </div>
        <Spin spinning={this.state.isLoading}>
          <Table
            columns={this.columns}
            dataSource={this.state.data}
            pagination={this.state.pagination}
            onChange={this.handleTableChange}
            rowKey="_id"
          />
        </Spin>
        <UserForm
          wrappedComponentRef={this.saveFormRef}
          mode={this.state.mode}
          record={this.state.record}
          visible={this.state.showForm}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        />
      </div>
    )
  }
}
