import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Divider, Input, Icon, Form, Popconfirm, Table, Typography, Spin, message } from 'antd';

import 'components/Currencies/Currencies.style.scss';

import ComponentForm from 'components/Currencies/Currencies.Form';
import { getAllCurrencies, createCurrency, updateCurrency, deleteCurrency } from 'services/currencies';

const { Text } = Typography;

export default class Currencies extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      showForm: false,
      mode: 'new',
      record: {},
      isLoading: false,
    };
  }
  
  static propTypes = {
  }

  loadData = async (search = {}) => {
    try {
      this.setState({ isLoading: true });
      await getAllCurrencies(({ data, pagination }) => {
        this.setState({ data, pagination, isLoading: false })
      });
    } catch (error) {
      console.error(error);
      message.error('Hubo un problema al cargar los productos, por favor vuelta a intentarlo.');
      this.setState({ isLoading: false });
    }
  }

  showForm = (mode, record = {}) => {
    this.setState({ showForm: true, mode, record });

    this.form.resetFields();
    this.form.setFieldsValue(record);
  }

  handleOk = (e) => {
    const { record } = this.state;
    this.form.validateFields((err, newValues) => {
      if (err) {
        message.warning('Verifique la informacion en el formulario.');
        return;
      }

      const data = { ...record, ...newValues };
      this.form.resetFields();
      this.setState({ showForm: false });
      this.state.mode === 'new' ?
        createCurrency(data, this.loadData) :
        updateCurrency(data, this.loadData);
    });
  }

  handleCancel = (e) => {
    this.setState({ showForm: false });
  }

  handleDelete = (id) => {
    // deletePart(id, this.loadProducts);
  }

  handleSearch = (e) => {
    e.preventDefault();
    const search = document.getElementById('search-term').value;
    this.loadProducts({ search });
  }

  handleTableChange = (pagination, filters, sorter) => {
    const search = document.getElementById('search-term').value;
    this.loadProducts({ search, ...pagination });
  }

  saveFormRef = (formRef) => {
    if (formRef) {
      this.form = formRef.props.form;
    }
  }

  columns = [{
    title: 'Nombre',
    dataIndex: 'name',
    key: 'name',
    render: (name, record) => <Link to={`/almacen/${record._id}`}>{name}</Link>
  }, {
    title: 'Simbolo',
    dataIndex: 'symbol',
    key: 'symbol',
  }, {
    title: 'Descripción',
    dataIndex: 'description',
    key: 'description',
  }, {
    title: 'Acciones',
    key: 'action',
    render: (text, record) => (
      <span className="table-actions">
        <Text onClick={() => this.showForm('edit', record)}>Editar</Text>
        <Divider type="vertical" />
        <Popconfirm
          placement="topRight"
          title={`Desea borrar el Producto ${record.modelCode}?`}
          onConfirm={() => this.handleDelete(record._id)}
          okText="Si"
          cancelText="No"
        >
          <Text type="danger">Borrar</Text>
        </Popconfirm>
      </span>
    ),
  }];

  componentDidMount() {
    this.loadData();
  }

  render() {
    return (
      <div className="form-grid">
        <Form layout="inline" onSubmit={this.handleSearch}>
          <Form.Item>
            <Input
              id="search-term"
              prefix={<Icon type="thunderbolt" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Buscar Moneda"
              style={{ width: 320 }}
              type="text"
            />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" icon="search">Buscar</Button>
          </Form.Item>
        </Form>
        <div className="actions">
          <Button
            icon="plus"
            type="primary"
            onClick={() => this.showForm('new')}
          >
            AGREGAR MONEDA
          </Button>
        </div>
        <Spin spinning={this.state.isLoading}>
          <Table
            columns={this.columns}
            dataSource={this.state.data}
            pagination={this.state.pagination}
            onChange={this.handleTableChange}
            rowKey="_id"
          />
        </Spin>
        <ComponentForm
          wrappedComponentRef={this.saveFormRef}
          mode={this.state.mode}
          record={this.state.record}
          visible={this.state.showForm}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        />
      </div>
    )
  }
}
