import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Divider, Form, Modal, Input, Icon, Upload } from 'antd';

import { beforeUpload } from 'utils/uploader';
import { formItemLayout } from 'utils/forms';

class ProductsForm extends Component {
  constructor(props) {
    super(props);
    this.state = { imageUrl: undefined, loading: false };
  }

  static propTypes = {
    mode: PropTypes.string,
    visible: PropTypes.bool,
    onOk: PropTypes.func,
    onCancel: PropTypes.func,
  }

  static defaultProps = {
    record: {}
  }

  handleUpload = (info) => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      const imageUrl = info.file.response.data.link;
      this.setState({
        imageUrl,
        loading: false,
      });

      return imageUrl;
    }
  }

  render() {
    const { visible, mode, onOk, onCancel, form } = this.props;
    const okText = mode === 'new' ? 'Guardar' : 'Actualizar';
    const title = mode === 'new' ? 'Crear Nuevo Producto' : `Producto ${this.props.record.modelCode}`
    const { getFieldDecorator } = form;
    const { loading } = this.state || {};
    const imageUrl = this.props.form.getFieldValue('image');

    const uploadButton = (
      <div>
        <Icon type={loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">{loading ? 'Subiendo' : 'Subir Imagen'}</div>
      </div>
    );

    return (
      <Modal
        title={title}
        style={{ top: 20 }}
        visible={visible}
        okText={okText}
        onOk={onOk}
        onCancel={onCancel}
      >
        <Form {...formItemLayout}>
          <Form.Item label="Codigo">
            {getFieldDecorator('modelCode', {
              rules: [{ required: true, message: 'Ingrese el codigo del producto.' }],
            })(
              <Input />
            )}
          </Form.Item>

          <Form.Item label="Nombre">
            {getFieldDecorator('name')(<Input />)}
          </Form.Item>

          <Form.Item label="Descripción">
            {getFieldDecorator('description')(<Input type="textarea" />)}
          </Form.Item>

          <Divider />

          <Form.Item label="Material">
            {getFieldDecorator('material')(<Input />)}
          </Form.Item>

          <Form.Item label="Color">
            {getFieldDecorator('color')(<Input />)}
          </Form.Item>

          <Form.Item label="Imagen">
            {getFieldDecorator('image', {
              getValueFromEvent: this.handleUpload,
            })(
              <Upload
                name="image"
                listType="picture-card"
                className="image-uploader"
                showUploadList={false}
                action="https://api.imgur.com/3/image"
                headers={{ 'Authorization': 'Client-ID 388082ce0008ee3' }}
                beforeUpload={beforeUpload}
              >
                {imageUrl ? <img src={imageUrl} alt="avatar" /> : uploadButton}
              </Upload>
            )}
          </Form.Item>
        </Form>
      </Modal>
    )
  }
}

export default Form.create({ name: 'product_form_modal' })(ProductsForm);
