import React, { Component } from 'react';
import { Button, Divider, Row, Col, Layout, Tabs, Table, Typography, message } from 'antd';
import { withRouter } from 'react-router-dom';
import { isEmpty, unionBy } from 'lodash';

import 'components/Products/Product.Detail.style.scss';

import { getPart, getAllRevisions, createRevision, updateRevision } from 'services/parts';
import { DEFAULT_IMAGE } from 'utils/forms';
import ComponentForm from 'components/Products/Product.Detail.Form';
import BinSearchForm from 'components/Bins/Bins.Search';

const { Content } = Layout;
const { Text } = Typography;
const TabPane = Tabs.TabPane;

class ProductDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: {},
      bins: { data: [] },
      mode: 'new',
      record: {},
      isLoading: false,
      showBinSearchForm: false,
      currentRevision: {},
    };
  }

  static propTypes = {
  }

  showForm = (mode, record = {}) => {
    this.setState({ showForm: true, mode, record });

    this.form.resetFields();
    this.form.setFieldsValue(record);
  }

  openBinSearch = (currentRevision) => {
    this.setState({ showBinSearchForm: true, currentRevision });
  }

  closeBinSearch = () => {
    this.setState({ showBinSearchForm: false });
  }

  addBins = async (selectedBins = []) => {
    selectedBins = selectedBins.map(bin => ({ bin }));
    let { currentRevision, currentRevision: { bins = [] } } = this.state;
    bins = unionBy(bins, selectedBins, 'bin');
    this.setState({ showBinSearchForm: false });

    await updateRevision({ ...currentRevision, bins }, this.loadProduct)
  }

  loadProduct = () => {
    const { match: { params: { id } } } = this.props;
    this.id = id;
    getPart(id, product => this.setState({ product }));
    getAllRevisions(id, bins => this.setState({ bins }))
  }

  productInfo = () => {
    const { product } = this.state;

    return (
      <Content className="product-info">
        <div className="product">
          <Divider>{product.modelCode}</Divider>
          <img src={product.image || DEFAULT_IMAGE} alt="product" />
        </div>

        <Divider dashed />

        <Text strong>Nombre</Text>
        <Text>{product.name}</Text>

        <Text strong>Descripción</Text>
        <Text>{product.description}</Text>

        <Text strong>Material</Text>
        <Text>{product.material}</Text>

        <Text strong>Color</Text>
        <Text>{product.color}</Text>
      </Content>
    )
  }

  revisionDetail = (bin) => {
    const columns = [{
      title: 'Deposito',
      dataIndex: 'bin.name',
      key: 'bin',
    }, {
      title: 'Cantidad',
      dataIndex: 'quantity',
      key: 'quantity',
    }];
    const { bins: dataSource } = bin;
    const isDataSourceEmpty = isEmpty(dataSource);

    return (
      <div className="product-detail">
        <div className="actions">
          <Button
            type="dashed"
            icon="plus"
            onClick={() => this.openBinSearch(bin)}
          >
            AGREGAR CANTIDAD
          </Button>
        </div>

        {!isDataSourceEmpty && <Table
          columns={columns}
          dataSource={dataSource}
          onChange={null}
          pagination={false}
          rowKey="_id"
          size="small"
        />}
      </div>
    )
  }

  revisions = () => {
    const { data: bins } = this.state.bins;

    return (
      <div className="part-revisions">
        <div className="actions">
          <Button
            type="primary"
            icon="plus"
            onClick={() => this.showForm('new')}
          >
            AGREGAR REVISION
          </Button>
        </div>

        {bins.map((bin, index) =>
          <div key={index} className="revision">
            <div className="information">
              <Row gutter={16}>
                <Col span={12}>
                  <Text strong>Nombre:</Text> {bin.name}
                </Col>
                <Col span={12}>
                  <Text strong>Precio:</Text>{bin.unitPrice || 0}
                </Col>

                <Col span={12}>
                  <Text strong>Descripción:</Text> {bin.description}
                </Col>
                <Col span={12}>
                </Col>
              </Row>
            </div>
            <Layout className="details">
              {this.revisionDetail(bin)}
            </Layout>
          </div>
        )}
      </div>
    )
  }

  handleOk = (e) => {
    const { record } = this.state;
    this.form.validateFields((err, newValues) => {
      if (err) {
        message.warning('Verifique la informacion en el formulario.');
        return;
      }

      const data = { part: this.id, ...record, ...newValues };
      this.form.resetFields();
      this.setState({ showForm: false });
      this.state.mode === 'new' ?
        createRevision(data, this.loadProduct) :
        updateRevision(data, this.loadProduct);
    });
  }

  handleCancel = (e) => {
    this.setState({ showForm: false });
  }

  saveFormRef = (formRef) => {
    if (formRef) {
      this.form = formRef.props.form;
    }
  }

  componentDidMount() {
    this.loadProduct();
  }

  render() {
    const { mode, record, showForm, showBinSearchForm } = this.state;

    return (
      <Row gutter={16}>
        <Col span={8}>
          <div className="form-grid">
            <this.productInfo />
          </div>
        </Col>
        <Col span={16}>
          <div className="form-grid">
            <Tabs defaultActiveKey="1">
              <TabPane tab="Revisiones" key="1">
                <this.revisions />
              </TabPane>
              <TabPane tab="Transacciones" key="2">
              </TabPane>
            </Tabs>
          </div>
        </Col>
        <ComponentForm
          wrappedComponentRef={this.saveFormRef}
          mode={mode}
          record={record}
          visible={showForm}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        />
        {showBinSearchForm && <BinSearchForm
          visible={showBinSearchForm}
          onOk={this.addBins}
          onCancel={this.closeBinSearch}
        />}
      </Row>
    )
  }
}

export default withRouter(ProductDetail)
