import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { Form, Modal, Input, Select } from 'antd';

import { formItemLayout } from 'utils/forms';
import { getAllCurrencies } from 'services/currencies';

const Option = Select.Option;

class ProductDetailForm extends Component {
  constructor(props) {
    super(props);
    this.state = { currencies: [], defaultCurrency: undefined };
  }

  static propTypes = {
  }

  loadCurrencies = () => {
    getAllCurrencies(({ data: currencies }) => {
      this.setState({ currencies });

      const { _id: defaultCurrency } = currencies.find(currency => currency.symbol.toLowerCase() === 'bs');
      this.setState({ defaultCurrency });
    });
  }

  componentDidMount = () => {
    this.loadCurrencies();
  }

  render() {
    const { visible, mode, onOk, onCancel, form } = this.props;
    const okText = mode === 'new' ? 'Guardar' : 'Actualizar';
    const title = mode === 'new' ? 'Crear Nueva Revision' : `Revision ${this.props.record.name}`
    const { getFieldDecorator } = form;
    const { currencies, defaultCurrency } = this.state;

    return (
      <Modal
        title={title}
        style={{ top: 20 }}
        visible={visible}
        okText={okText}
        onOk={onOk}
        onCancel={onCancel}
      >
        <Form {...formItemLayout}>
          <Form.Item hasFeedback label="Nombre">
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Ingrese el nombre de la Revision.' }],
            })(
              <Input />
            )}
          </Form.Item>

          <Form.Item label="Descripción">
            {getFieldDecorator('description')(<Input type="text" />)}
          </Form.Item>

          <Form.Item label="Precio">
            {getFieldDecorator('unitPrice')(<Input type="text" />)}
          </Form.Item>

          <Form.Item label="Moneda">
            {getFieldDecorator('currency', {
              initialValue: defaultCurrency
            })(
              <Select>
                {currencies.map(({ _id, symbol, name }, index) =>
                  <Option key={_id} value={_id}>
                    {`${symbol} ${name}`}
                  </Option>
                )}
              </Select>
            )}
          </Form.Item>
        </Form>
      </Modal>
    )
  }
}

export default Form.create({ name: 'product_detail_form_modal' })(ProductDetailForm);