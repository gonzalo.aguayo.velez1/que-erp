import type from './types';

export const form = {
  ID: {
    description: 'Número Cliente',
    autofocus: true,
    controls: [{
      name: 'numberClient',
      description: 'Número',
      tooltip: 'El número del cliente puede ser su CI o teléfono.',
      isRequired: true,
      type: type.TEXT,
    }]
  },
  Information: {
    description: 'Información',
    controls: [{
      name: 'name',
      description: 'Nombre',
      isRequired: true,
      type: type.TEXT,
    }, {
      name: 'fatherSurname',
      description: 'Apellido Paterno',
      isRequired: false,
      type: type.TEXT,
    }, {
      name: 'motherSurname',
      description: 'Apellido Materno',
      isRequired: false,
      type: type.TEXT,
    }]
  },
  Address: {
    description: 'Dirección',
    controls: [{
      name: 'addressLine1',
      description: 'Dirección',
      isRequired: false,
      type: type.TEXT,
    }, {
      name: 'addressLine2',
      description: 'Dirección',
      isRequired: false,
      type: type.TEXT,
    }]
  },
  Contact: {
    description: 'Contacto',
    controls: [{
      name: 'phone',
      description: 'Teléfono',
      isRequired: false,
      type: type.TEXT,
    }]
  }
};

export const model = {
  path: '/clientes',
  resource: 'clients',
  description: 'Clientes',
  singleDescription: 'Cliente',
  keys: ['numberClient'],
  columns: [{
    title: 'Nro',
    dataIndex: 'numberClient',
  }, {
    title: 'Nombre',
    dataIndex: 'name',
  }, {
    title: 'Ap. Paterno',
    dataIndex: 'fatherSurname',
  }, {
    title: 'Ap. Materno',
    dataIndex: 'motherSurname',
  }, {
    title: 'Direccion',
    dataIndex: 'addressLine1',
  }, {
    title: '',
    dataIndex: 'addressLine2',
  }, {
    title: 'Teléfono',
    dataIndex: 'phone',
  }]
};

export default { form, model };
