export default {
  BOOLEAN: 'boolean',
  DATE: 'date',
  DATETIME: 'datetime',
  TEXT: 'text',
  IMAGE: 'image'
}
