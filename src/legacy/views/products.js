import type from './types';

export const form = {
  Code: {
    description: 'Información',
    autofocus: true,
    controls: [{
      name: 'modelCode',
      description: 'Modelo',
      isRequired: true,
      type: type.TEXT,
    }, {
      name: 'name',
      description: 'Nombre',
      isRequired: true,
      type: type.TEXT,
    }]
  },
  Description: {
    description: 'Descripcion',
    controls: [{
      name: 'description',
      description: 'Descripcion del producto',
      type: type.TEXT,
      size: 'l',
    }]
  },
  Details: {
    description: 'Detalles',
    controls: [{
      name: 'material',
      description: 'Material',
      type: type.TEXT,
    }, {
      name: 'color',
      description: 'Color',
      type: type.TEXT,
    }, {
      name: 'shape',
      description: 'Forma',
      type: type.TEXT,
    }]
  },
  Image: {
    description: 'Imagen',
    autofocus: true,
    controls: [{
      name: 'imageURL',
      type: type.IMAGE,
      size: 'l',
    }]
  },
};

export const model = {
  path: '/productos',
  resource: 'parts',
  description: 'Productos',
  singleDescription: 'Producto',
  keys: ['modelCode'],
  columns: [{
    title: 'Codigo',
    dataIndex: 'modelCode',
  }, {
    title: 'Nombre',
    dataIndex: 'name',
  }],
};

export default { form, model };
