const grid = (state = {}, action) => {
  switch (action.type) {
    case 'SET_GRID_DATA':      
      return {
        ...state,
        data: action.data || [],
      }
  
    default:
      return state;
  }
};

export default grid;
