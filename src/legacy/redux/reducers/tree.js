import { uniqBy } from 'lodash';

const initialState = {
  model: undefined,
  currentNode: undefined,
  selectedKeys: [],
  children: [],
};

const tree = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_MODEL':      
      return {
        ...state,
        model: action.model,
        children: [],
      };
    
    case 'TREE_ADD_CHILD':
      return {
        ...state,
        children: uniqBy([...state.children, action.child], 'id')
      };
    case  'TREE_SELECTED_KEYS':
      return {
        ...state,
        selectedKeys: action.selectedKeys
      };

    default:
      return state;
  }
};

export default tree;
