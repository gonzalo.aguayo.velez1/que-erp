const initialState = {};

const form = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_VALUE':      
      return {
        ...state,
        ...action.values
      };
    
    case 'CLEAR_VALUES':
      return {};
  
    default:
      return state;
  }
};

export default form;
