import { combineReducers } from 'redux';

import tree from './tree';
import form from './form';
import grid from './grid';

export default combineReducers({
  tree,
  form,
  grid
});
