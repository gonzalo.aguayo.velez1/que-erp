export const getTree = store => store.tree;

export const getFormValue = store => store.form

export const getGridData = store => store.grid.data;
