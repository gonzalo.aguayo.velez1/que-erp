export const setTreeModel = (model) => ({
  type: 'SET_MODEL',
  model
});

export const addChildToTree = (child) => ({
  type: 'TREE_ADD_CHILD',
  child
});

export const treeSetSelectedKeys = (selectedKeys) => ({
  type: 'TREE_SELECTED_KEYS',
  selectedKeys
});

export const setFormValue = (values) => ({
  type: 'SET_VALUE',
  values
});

export const clearFormValues = () => ({
  type: 'CLEAR_VALUES',
});

export const setGridData = (data) => ({
  type: 'SET_GRID_DATA',
  data,
});
