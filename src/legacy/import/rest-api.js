import request from 'request-promise';
import { isEmpty } from 'lodash';

const BASE_URL = 'http://127.0.0.1:3001/api/v1';

const getURL = (url, params) => {
  const objectParams = params || {};
  const queryParams = Object.keys(objectParams).map(key => key + '=' + objectParams[key]).join('&');

  return isEmpty(queryParams) ? url : `${url}?${queryParams}`;
};

export default {
  get: async (path, params) => {
    const method = 'GET';
    const uri = getURL(`${BASE_URL}/${path}`, params);
    const json = true;
    const options = { method, uri, json };

    return await request(options);
  },

  post: async (path, params, body) => {
    const method = 'POST';
    const uri = getURL(`${BASE_URL}/${path}`, params);
    const json = true;
    const options = { method, uri, json, body };

    return await request(options);
  },

  delete: async (path, id, params) => {
    const method = 'DELETE';
    const uri = getURL(`${BASE_URL}/${path}/${id}`, params);
    const json = true;
    const options = { method, uri, json };

    return await request(options);
  },

  put: async (path, id, params, body) => {
    const method = 'PUT';
    const uri = getURL(`${BASE_URL}/${path}/${id}`, params);
    const json = true;
    const options = { method, uri, json, body };

    return await request(options);
  }
}
