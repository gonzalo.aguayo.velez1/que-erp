export const FORM = 'form';
export const GRID = 'grid';
export const BUSINESS = 'business';

export default {FORM, GRID, BUSINESS};
