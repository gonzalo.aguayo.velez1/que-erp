import { pick } from 'lodash';

export const getTreeChild = (data, model) => {
  const child = {
    id: data._id,
    keys: model.keys,
    data: pick(data, [...model.keys, '_id']),
    title: model.singleDescription
  };

  return child;
}
