import * as Views from '../views';
import colors from './colors';
import * as ViewType from '../import/view-types';

// Business Components
import { SalesOrderForm } from '../components'; 

export const routes = [{
  icon: 'shop',
  text: 'Nueva Orden',
  path: '/nueva-orden',
  tone: colors.green,
  view: {
    type: ViewType.BUSINESS,
    Component: SalesOrderForm
  }
}, {
  icon: 'contacts',
  text: 'Clientes',
  path: '/clientes',
  tone: colors.purple,

  options: [{
    path: '/editar/:id',
    hidden: true,
    view: {
      type: ViewType.FORM,
      content: Views.Clients
    },
  }, {
    text: 'Lista Clientes',
    path: '/lista',
    view: {
      type: ViewType.GRID,
      content: Views.Clients.model
    }
  }, {
    text: 'Nuevo Cliente',
    path: '/nuevo',
    view: {
      type: ViewType.FORM,
      content: Views.Clients
    }
  }]
}, {
  icon: 'hdd',
  text: 'Productos',
  path: '/productos',
  tone: colors.orange,

  options: [{
    path: '/editar/:id',
    hidden: true,
    view: {
      type: ViewType.FORM,
      content: Views.Products
    },
  }, {
    text: 'Productos',
    path: '/lista',
    view: {
      type: ViewType.GRID,
      content: Views.Products.model
    }
  }, {
    text: 'Nuevo Producto',
    path: '/nuevo',
    view: {
      type: ViewType.FORM,
      content: Views.Products
    }
  }, {
    text: 'Precios',
    path: '/precios'
  }, {
    text: 'Promociones',
    path: '/promociones'
  }]
}, {
  icon: 'file-text',
  text: 'Pedidos',
  path: '/pedidos',
  tone: '#1890ff',

  options: [{
    text: 'Nuevos',
    path: '/'
  }, {
    text: 'Cancelados',
    path: '/'
  }]
}, {
  icon: 'hdd',
  text: 'Trabajos',
  path: '/trabajos',
  tone: '#1890ff',
}, {
  icon: 'schedule',
  text: 'HR',
  path: '/hr',
  tone: '#1890ff',
}, {
  icon: 'setting',
  text: 'Ajustes',
  path: '/ajustes',
  tone: '#1890ff',
}];
