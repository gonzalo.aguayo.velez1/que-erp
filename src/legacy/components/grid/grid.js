import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Divider, Typography, message, Table, Modal } from 'antd';

import restApi from '../../import/rest-api';

import './grid.style.scss';

const { Text } = Typography;

class Grid extends Component {
  constructor(props) {
    super(props);

    this.state = { data: [] };
    this.loadData();
  }

  async loadData() {
    try {
      const { resource } = this.props;
      const { data } = await restApi.get(resource, {});
      
      this.setState({ data })
    } catch (error) {
      message.error('Hubo un error al cargar la información.');
      console.error(error);
    }
  }

  deleteRecord(record) {
    const { singleDescription, keys, resource } = this.props;

    Modal.confirm({
      title: `Desea eliminar la informacion del ${singleDescription}?`,
      content: `${keys.map(key => record[key]).join(' - ')}`,
      okText: 'Si',
      okType: 'danger',
      cancelText: 'No',
      onOk: async () => {
        await restApi.delete(resource, record._id, {});
        this.loadData();
      },
      onCancel: () => {
      },
    });
  }

  actionColumn = {
    title: 'Acciones',
    key: 'action',
    render: (text, record) => (
      <span>
        <Link to={`${this.props.path}/editar/${record._id}`}>Editar</Link>
        <Divider type="vertical" />
        <Text className="action" type="danger" onClick={() => this.deleteRecord(record)}>Borrar</Text>
      </span>
    ),
  };

  render() {
    let { columns = [] } = this.props;
    const { data = [] } = this.state;
    const dataSource = data.map((row, key) => ({ ...row, key }));
    columns = [...columns, this.actionColumn];

    return (
      <div className="que-grid">
        <Table
          columns={columns}
          dataSource={dataSource}
        />
      </div>
    )
  }
}

export default Grid;
