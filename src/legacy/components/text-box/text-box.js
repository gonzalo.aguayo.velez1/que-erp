import React, { Component } from 'react'
import { Input, Typography, Tooltip } from 'antd';

import 'antd/lib/input/style/css';
import './text-box.style.scss';

const { Text } = Typography;

export default class TextBox extends Component {
  onChange = (e) => {
    const { value } = e.target;
    this.props.setValue(this.props.name, value);
  }

  size = {
    s: '200px',
    m: '400px',
    l: '600px',
    xl: '100%',
  }

  render() {
    return (
      <div className={`que-text-box ${this.props.isRequired ? 'required' : 'optional'}`}>
        <Tooltip placement="top" title={this.props.tooltip}>
          <Text>{this.props.description}</Text>
        </Tooltip>
        
        <Input
          id={this.props.name}
          type="text"
          value={this.props.value}
          onChange={this.onChange}
          style={{ width: this.size[this.props.size] || this.size.s }}
          className={this.props.checkAsRequired ? 'required' : ''}
        />
      </div> 
    )
  }
}
