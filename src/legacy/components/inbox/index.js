import React, { Component } from 'react';

import { Icon } from 'antd';

import './inbox.style.scss';

export default class Inbox extends Component {
  render() {
    return (
      <div className="que-inbox">
        <Icon type="inbox" />
      </div>
    )
  }
}
