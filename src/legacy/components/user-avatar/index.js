import React, { Component } from 'react';
import { Avatar, Badge } from 'antd';

import './user-avatar.style.scss';

export default class UserAvatar extends Component {
  render() {
    return (
      <div className="que-avatar">
        <span className="pic">
          <Badge dot><Avatar shape="square" icon="user" /></Badge>
        </span>
        <div className="user">
          <span className="name">
            John Doe
          </span>
          <span className="role">
            Administrador
          </span>
        </div>
      </div>
    )
  }
}
