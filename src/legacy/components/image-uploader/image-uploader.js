import React, { Component } from 'react';
import { Upload, Icon, message } from 'antd';

import './image-uploader.scss';

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file) {
  const isValid = file.type === 'image/jpeg' || file.type === 'image/png';
  
  if (!isValid) {
    message.error('El formato de la imagen no es valido.');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('La image no debe ser mas grande que 2Mb.');
  }

  return isValid && isLt2M;
}


export default class ImageUploader extends Component {
  constructor(props){
    super(props);

    this.state = {
      loading: false
    };
  }

  handleChange = (info) => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl => this.setState({
        imageUrl: imageUrl,
        loading: false,
      }));
    }
    if (info.file.response.success) {
      this.props.setValue(this.props.name, info.file.response.data.link);
    }
  }

  render() {
    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">Subir</div>
      </div>
    );
    const imageUrl = this.state.imageUrl || this.props.value;
    return (
      <Upload
        name="image"
        listType="picture-card"
        className="image-uploader"
        showUploadList={false}
        action="https://api.imgur.com/3/image"
        headers={{'Authorization': 'Client-ID 388082ce0008ee3'}}
        beforeUpload={beforeUpload}
        onChange={this.handleChange}
      >
        {imageUrl ? <img src={imageUrl} alt="avatar" /> : uploadButton}
      </Upload>
    );
  } 
}
