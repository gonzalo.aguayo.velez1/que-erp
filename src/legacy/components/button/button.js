import React, { Component } from 'react'

import { Button } from 'antd';


export default class Command extends Component {
  render() {
    return (
      <Button type="primary">
        {this.props.children}
      </Button>
    )
  }
}
