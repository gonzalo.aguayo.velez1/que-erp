import React, { Component } from 'react';
import { Modal, message, Button, Typography } from 'antd';
import { values, concat, isEmpty } from 'lodash';

import { QueGroup } from '../index';
import type from '../../views/types';
import { QueTextBox, QueImageUploader } from '../index';
import restApi from '../../import/rest-api';

import './form-editor.style.scss';
import { isFunction } from 'util';

const { Title } = Typography;

class FormEditor extends Component {
  constructor(props) {
    super(props);

    const { form = [], model = {} } = this.props;
    this.state = {
      model,
      isSaving: false,
      form: values(form),
      value: {},
      required: {},
      title: ''
    };
  }

  validateForm = () => {
    const validations = [];
    const required = {};
    const controls = this.state.form.map(({controls}) => controls).reduce((acc, cur) => [...acc, ...cur], []);
    
    // Required fields
    const requiredControls = controls.filter(({isRequired}) => isRequired);
    let allRequiredAreValid = true;

    requiredControls.forEach(({name, description}) => {
      const isValid = !isEmpty(this.state.value[name]);

      if (!isValid) {
        const message = `El campo ${description} es Requerido.`;
        required[name] = true;
        validations.push(message);
        allRequiredAreValid = false;
      }
    });

    // Message Validations
    if (validations.length > 0) {
      Modal.warning({
        title: 'Verifique su información antes de enviarla',
        content: validations.map((message, index) => <p key={index}>{message}</p>),
      });
    }

    this.setState({required});
    return allRequiredAreValid;
  }

  save = async () => {
    const isValid = this.validateForm();

    if (!isValid) { return }

    const params = {};
    this.setState({ isSaving: true });

    if (this.state.value._id) {
      await restApi.put(this.props.model.resource, this.state.value._id, params, this.state.value);
      message.success(`${this.props.model.singleDescription} ha sido actualizado.`);
    } else {
      await restApi.post(this.props.model.resource, params, this.state.value);
      message.success(`${this.state.model.singleDescription} ha sido creado.`);
      this.clearAndFocus();
    }

    if (isFunction(this.props.onSave)) {
      this.props.onSave();
    }

    this.setState({ isSaving: false });
  }

  clearAndFocus = () => {
    this.setState({value: {}});
  }

  setValue = (field, newValue) => {
    const updatedProperty = {[field]: newValue};
    const value = {...this.state.value, ...updatedProperty};
    this.setState({value});
  }

  Controls = ({ controls = [] }) => {
    return controls.map((control, index) => {
      let DomControl = null;      

      switch (control.type) {
        case type.TEXT:
          DomControl = <QueTextBox
            key={index}
            value={this.state.value[control.name]}
            setValue={this.setValue}
            checkAsRequired={this.state.required[control.name]}
            {...control} 
          />;
          break;

        case type.IMAGE:
          DomControl = <QueImageUploader
            key={index}
            value={this.state.value[control.name]}
            setValue={this.setValue}
            {...control}
          />  
        break;
      
        default:
          break;
      }

      return DomControl;
    })
  }

  loadTitle = () => {
    const { id } = this.props.match.params;
    const title = id ?
      `${this.props.model.singleDescription} ${this.props.model.keys.map(key => this.state.value[key]).join(' - ')}` :
      `Nuevo ${this.props.model.singleDescription}`;

    this.setState({title})
  }

  loadData() {
    const { id } = this.props.match.params;

    if (id) {
      const { resource } = this.props.model;
      const uri = `${resource}/${id}`;
      restApi.get(uri)
        .then(({ data: value }) => {
          this.setState({value});
        })
        .then(this.loadTitle)
    }
  }

  Options = () => {
    return (
      <div className="options">
        <Button
          type="primary"
          size="large"
          shape="round"
          icon="save"
          onClick={this.save}
          loading={this.state.isSaving}
        >
          Guardar
        </Button>
      </div>)
  }

  componentDidMount() {
    this.loadData();
  }
  
  render() {
    return (
      <section className="que-form-editor">
        {
          !this.props.showOptionsBottom &&
          <this.Options />
        }
        <form className="editor">
          <Title level={2}>{this.state.title}</Title>
          {this.state.form.map((group, index) =>
            <QueGroup key={index} {...group}>
              <this.Controls {...group} />
            </QueGroup>
          )}
        </form>
        {
          this.props.showOptionsBottom &&
          <this.Options />
        }
      </section>
    )
  }
}

export default FormEditor;
