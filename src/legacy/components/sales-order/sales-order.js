import React, { Component } from 'react';
import { AutoComplete, Button, Collapse,
  Input, Card, Icon, Table, Modal, Tabs, List, Avatar, Typography, Select } from 'antd';

import restApi from '../../import/rest-api';
import { QueFormEditor } from '../index';
import { Clients } from '../../views';

import './sales-order.style.scss';
import 'antd/lib/collapse/style/css';

const Search = Input.Search;
const Panel = Collapse.Panel;
const { Meta } = Card;
const Option = AutoComplete.Option;
const TabPane = Tabs.TabPane;
const { Title } = Typography;

export default class SalesOrder extends Component {
  constructor(props) {
    super(props);

    this.state = {
      products: [],
      customers: [],
      customer: undefined,
      selectedProducts: [],
      modalVisible: false,
      dataSource: [
        { _id: 'new' }
      ]
    };

    this.loadProducts();
  }

  loadProducts = async () => {
    const { data: products } = await restApi.get('parts');
    this.setState({ products });
  }

  addProduct = (product) => {
    const { selectedProducts } = this.state;
    selectedProducts.push(product);

    this.setState({ selectedProducts });
  }

  handleSelect = (value) => {
    if (value === 'new') {
      const config = {
        text: 'Clientes',
        path: '/clientes',
        view: Clients,
        match: {
          params: {
            id: 'nuevo'
          }
        }
      };

      this.ModalContent = (<section className="que-form">
        <QueFormEditor {...config} />
      </section>);
      this.setState({ modalVisible: true });
    }
  }

  Products = () => {
    const {products} = this.state;
    const columns = [{
      title: '',
      dataIndex: 'imageURL',
      key: 'imageURL',
      render: url => <img className="product image" src={url} alt="" />
    }, {
      title: 'Producto',
      dataIndex: 'name',
      key: 'name'
    }, {
      title: 'Descripcion',
      dataIndex: 'description',
      key: 'description'
    }, {
      title: '',
      dataIndex: 'action',
      key: 'action',
      render: (text, record) =>
        <Button
          type="primary"
          shape="round"
          icon="plus"
          onClick={() => this.addProduct(record)}
        >
          Agregar
        </Button>
    }];

    return (
      <Table className="products" columns={columns} dataSource={products}></Table>
    );
  }

  Recipe = () => {
    return null;
  }

  handleOk = () => {
    this.setState({ modalVisible: false });
    this.ModalContent = null;
  }

  handleCancel = () => {
    this.setState({ modalVisible: false });
    this.ModalContent = null;
  }

  handleSearch = () => {
    restApi.get('clients').then(({data: customers}) => this.setState({customers}));
  }

  handleChange = (value) => {
    if (value !== 'new') {
      this.setState({customer: value});
    } else {
      this.setState({modalVisible: true});
    }
  }

  afterCreateNewCustomer = () => {
    this.setState({modalVisible: false});
    this.handleSearch();
  }

  render() {
    const options = this.state.customers.map(({_id, name, fatherSurname, motherSurname}) =>
      <Option key={_id}>{`${name} ${fatherSurname || ''} ${motherSurname || ''}`}</Option>);
    options.push(<Option key="new">+ Registrar Nuevo</Option>)

    return (
      <div className="sales-order">
        <Tabs className="options" defaultActiveKey="1">
          <TabPane tab="Productos" key="1">
            <this.Products />
          </TabPane>

          <TabPane tab="Receta" key="2" disabled={true}>
            <this.Recipe />
          </TabPane>
        </Tabs>

        <div className="resume">
          <Title level={4}>Cliente</Title>
          <Select
            className="customer-search"
            showSearch
            value={this.state.customer}
            placeholder="Cliente"
            defaultActiveFirstOption={false}
            showArrow={false}
            filterOption={false}
            onSearch={this.handleSearch}
            onChange={this.handleChange}
            notFoundContent={null}
            size="large"
          >
            {options}
          </Select>

          <Title level={4}>Seleccionados</Title>
          <List
            itemLayout="horizontal"
            dataSource={this.state.selectedProducts}
            renderItem={item => (
              <List.Item>
                <List.Item.Meta
                  avatar={<Avatar src={item.imageURL} />}
                  title={item.name}
                  description={item.description}
                />
              </List.Item>
            )}
          />
        </div>

        <Modal
          title="Registrar Nuevo Cliente"
          visible={this.state.modalVisible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          cancelText="Cancelar"
          width="60%"
          footer={null}
        >
          <QueFormEditor
            {...Clients}
            match={{params: {}}}
            onSave={this.afterCreateNewCustomer}
            showOptionsBottom={true}
          />
        </Modal>
      </div>
    )
  }
}
