import React, { Component } from 'react';
// import 'particles.js';

// import { default as particleConfig } from '../../import/third-party/particles/config';
import { QueUserAvatar } from '../index';

import './top-bar.style.scss';

export default class TopBar extends Component {
  componentDidMount() {
    // eslint-disable-next-line no-undef
    // particlesJS('que-top-bar', particleConfig);
  }

  render() {
    return (
      <header className="header navbar">
        <span className="menu" onClick={() => this.props.onClickMenu()}>
          <i className="fas fa-bars"></i>
        </span>

        <QueUserAvatar />
      </header>
    )
  }
}
