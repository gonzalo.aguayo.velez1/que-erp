import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Tree, Icon } from 'antd';
import { isEmpty } from 'lodash';

import { getTree } from '../../redux/selectors';
import { clearFormValues, setFormValue, treeSetSelectedKeys } from '../../redux/actions';

import './tree.style.scss';
import restApi from '../../import/rest-api';

const { TreeNode } = Tree;

class TreeComponent extends Component {
  constructor(props) {
    super(props);

    this.state = { model: {} };
  }

  goTo(selectedKeys) {
    let [currentKey] = selectedKeys;

    if (!currentKey) {
      return;
    }

    this.props.clearFormValues({});

    if (currentKey !== 'nuevo' && currentKey !== 'root') {
      const { model } = this.state;
      const uri = `${model.resource}/${currentKey}`;
      restApi.get(uri).then(({ data: value }) => {
        this.props.setFormValue(value);
      });
    }

    currentKey = currentKey === 'root' ? '' : `/${currentKey}`;
    const path = `${this.state.model.path}${currentKey}`;

    this.props.history.push(path);
    this.props.treeSetSelectKeys(selectedKeys);
  }

  getTitle(child) {
    let subtitle = child.keys.map(key => child.data[key]).join(' - ');

    return `${child.title} - ${subtitle}`;
  }

  componentDidMount() {
    const { model = {} } = this.props.view || {};
    
    this.setState({ model });
  }

  render() {
    const { tree: { children = [] } } = this.props;

    return (
      <div className="que-tree">
        <Tree
          showIcon
          defaultExpandAll
          switcherIcon={<Icon type="down" />}
          selectedKeys={this.props.tree.selectedKeys}
          onSelect={(selectedKeys) => this.goTo(selectedKeys)}
        >
          <TreeNode
            key="root"
            icon={<Icon type="home" />}
            title={this.state.model.description}
            disabled={isEmpty(this.state.model.resource)}
          >
            <TreeNode
              key="nuevo"
              icon={<Icon type="plus-square" />}
              title="Nuevo"
              disabled={isEmpty(this.state.model.resource)}
            />
            {children.map(child =>
              <TreeNode
                key={child.id}
                icon={<Icon type="form" />}
                title={this.getTitle(child)}
              />
            )}
          </TreeNode>
        </Tree>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  tree: getTree(state),
});

const mapDispatchToProps = dispatch => ({
  clearFormValues: () => dispatch(clearFormValues()),
  setFormValue: (value) => dispatch(setFormValue(value)),
  treeSetSelectKeys: (selectedKeys) => dispatch(treeSetSelectedKeys(selectedKeys)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TreeComponent);
