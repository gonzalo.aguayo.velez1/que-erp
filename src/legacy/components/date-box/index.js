import React, { Component } from 'react'
import { DatePicker } from 'antd';

import 'antd/lib/date-picker/style/css';

export default class DateBox extends Component {
  constructor(props) {
    super(props);

    this.dateFormat = 'DD/MM/YYYY';
    this.state = { value: '' };
  }
  
  onChange = (date, dateString) => {
    this.setState({ value: date })
  }

  render() {
    return (
      <DatePicker
        placeholder=""
        format={this.dateFormat}
        onChange={this.onChange}
      />
    )
  }
}
