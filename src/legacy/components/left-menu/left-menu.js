import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { Icon } from 'antd';

import { setTreeModel } from '../../redux/actions';

import './left-menu.style.scss';

class LeftMenu extends Component {
  clear() {
    this.props.setTreeModel({});
  }

  renderSubOptions(options = []) {
    return (
      <ul>
      {options.map(option => 
        <li>{option.text}</li>
      )}
      </ul>)
  }

  render() {
    return (
      <nav className="que-left-menu">
        {this.props.routes.map(({ icon, text, path, redirect, tone, options }, key) => 
          <NavLink
            to={redirect || path}
            key={`item-${key}`}
            onClick={() => this.clear()}
            activeClassName="active"
            className="option"
            tone={tone}
          >
            <Icon type={icon} theme="twoTone" twoToneColor={tone} />
            <span>{text}</span>
          </NavLink>
        )}

        <div className="sub-options">
          {this.renderSubOptions()}
        </div>
      </nav>
    )
  }
}


const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
  setTreeModel: (model) => dispatch(setTreeModel(model)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LeftMenu);
