import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Menu } from 'antd';

import './sub-options.style.scss';

export default class SubOptions extends Component {
  render() {
    const options = (this.props.options || []).filter(({hidden}) => !hidden);

    return (
      <Menu className="sub-options">
        {options.map((option, key) =>
        <Menu.Item key={key}>
          <NavLink
            to={`${this.props.path}${option.path}`}
            activeClassName="active"
          >
            {option.text}
          </NavLink>
        </Menu.Item>)}
      </Menu>
    )
  }
}
