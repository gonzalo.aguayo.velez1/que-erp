import React, { Component } from 'react';
import { Icon, Divider } from 'antd';
import { uniqueId } from 'lodash';

import './group.style.scss';

export default class Group extends Component {
  constructor(props) {
    super(props);

    this.id = uniqueId('group-');
    this.state = {
      isCollapsed: false
    };
  }

  collapse = () => {
    const { isCollapsed } = this.state;

    this.setState({ isCollapsed: !isCollapsed });
  }

  render() {
    return (
      <section id={this.id} className="que-group">
        {/* <div className="header" onClick={() => this.collapse()}>
          <h5 className="title">{this.props.description}</h5>
          <Icon type="down-circle" />
        </div> */}

        <Divider
          orientation="left" 
          onClick={this.collapse}
          className="divider"
        >
          <Icon type={`${this.state.isCollapsed ? 'up' : 'down'}-circle`} /> {this.props.description}
        </Divider>

        <div className={`controls ${this.state.isCollapsed ? 'collapsed' : 'open'}`}>
          {this.props.children}
        </div>
      </section>
    )
  }
}
