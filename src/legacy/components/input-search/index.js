import React, { Component } from 'react'
import { Input } from 'antd';

import './input-search.style.scss';

const Search = Input.Search;

export default class InputSearch extends Component {
  constructor(props) {
    super(props);
    this.state = { value: '' };
  }

  onSearch = (value) => {
    this.setState({ value });
  }
  
  render() {
    return (
      <div className="que-input-search">
        <Search
          placeholder={this.props.placeholder}
          onSearch={this.onSearch}
        />
      </div>
    )
  }
}
