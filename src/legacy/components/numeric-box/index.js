import React, { Component } from 'react'
import { Input } from 'antd';
import 'antd/lib/input/style/css';

export default class NumericBox extends Component {
  constructor(props) {
    super(props);
    this.state = { value: 0 };
  }

  onChange = (e) => {
    const { value } = e.target;
    const reg = /^-?(0|[1-9][0-9]*)(\.[0-9]*)?$/;

    if ((!Number.isNaN(value) && reg.test(value)) || value === '' || value === '-') {
      this.setState({ value });
    }
  }

  // '.' at the end or only '-' in the input box.
  onBlur = () => {
    const { value } = this.state;
    
    if (value.charAt(value.length - 1) === '.' || value === '-') {
      this.onChange(value.slice(0, -1));
    }

    if (`${value}` === '') {
      this.setState({ value: 0 });
    }
  }
  
  render() {
    return (
      <Input
        type="number"
        value={this.state.value}
        onChange={this.onChange}
        onBlur={this.onBlur}
        maxLength={25}
      />
    )
  }
}
