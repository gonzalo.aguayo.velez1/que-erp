import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Icon } from 'antd';

import './sidebar.style.scss';

export default class Sidebar extends Component {
  render() {
    return (
      <div className="sidebar">
        <div className="logo">

        </div>

        <ul className="menu scrollable">
          {this.props.routes.map(({ icon, text, path, redirect, tone, options }, key)  => 
          <li className="nav-item" key={`item-${key}`}>
            <NavLink
              to={redirect || path}
              onClick={() => this.clear()}
              activeClassName="active"
              className="option"
              tone={tone}
            >
              <Icon type={icon} theme="twoTone" twoToneColor={tone} />
              <span>{text}</span>
            </NavLink>
          </li>)}
        </ul>
      </div>
    )
  }
}
