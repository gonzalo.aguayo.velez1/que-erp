import { message } from 'antd';

export const beforeUpload = (file) => {
  const isJPG = file.type === 'image/jpeg';
  const isPNG = file.type === 'image/png';
  const isGIF = file.type === 'image/gif';
  const isTif = file.type === 'image/tif';
  const isValid = isJPG || isPNG || isGIF || isTif;

  if (!isValid) {
    message.error('La imagen no es soportada.');
  }

  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('La imagen no puede ser mas grande que 2MB!');
  }

  return isValid && isLt2M;
};

export const getBase64 = (img) => {
  const reader = new FileReader();
  return new Promise(resolve => {
    reader.addEventListener('load', () => resolve(reader.result));
    reader.readAsDataURL(img);
  });
};