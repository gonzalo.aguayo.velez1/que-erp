import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Divider, Layout, Breadcrumb, Icon } from 'antd';

import 'antd/dist/antd.css';
import './app.scss';

import SidebarMenu from 'components/SidebarMenu/SidebarMenu';
import Users from 'components/Users/Users';
import Products from 'components/Products/Products';
import ProductDetail from 'components/Products/Product.Detail';
import Warehouses from 'components/Warehouses/Warehouses';
import Bins from 'components/Bins/Bins';
import Currencies from 'components/Currencies/Currencies';
import PointOfSale from 'components/POS/PointOfSale';

const { Header, Content, Footer } = Layout;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { collapsed: false };
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }

  Breadcrumb = () => {
    return (
      <Breadcrumb style={{ margin: '16px 0' }}>
        <Breadcrumb.Item>Inicio</Breadcrumb.Item>
        <Breadcrumb.Item>Lista</Breadcrumb.Item>
      </Breadcrumb>
    )
  }

  Header = () => {
    return (
      <Header style={{ background: '#fff', padding: 0 }}>
        <Icon
          className="trigger"
          type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
          onClick={this.toggle}
        />
      </Header>
    )
  }

  Footer = () => {
    return (
      <Footer style={{ textAlign: 'center' }}>
        <Divider />
        © 2019 QUE ERP un producto de Qué.
      </Footer>
    )
  }

  render() {
    const pages = [{
      path: '/',
      exact: true,
      component: () => <div>Home!</div>
    }, {
      path: '/usuarios',
      component: Users,
    }, {
      path: '/productos',
      component: Products,
    }, {
      path: '/almacenes',
      component: Warehouses,
    }, {
      path: '/almacen/:id/',
      component: Bins
    }, {
      path: '/producto/:id',
      component: ProductDetail
    }, {
      path: '/monedas',
      component: Currencies
    }, {
      path: '/pos',
      component: PointOfSale
    }];

    return (
      <div className="app">
        <Layout>
          <Router>
            <SidebarMenu
              collapsed={this.state.collapsed}
            />
            <Layout>
              <this.Header />
              <Content style={{ margin: '0 16px', overflowY: 'auto' }}>
                <this.Breadcrumb />
                <div className="content">
                  {pages.map((page, index) =>
                    <Route
                      key={index}
                      exact={page.exact}
                      path={page.path}
                      component={page.component}
                    />
                  )}
                </div>
              </Content>
              <this.Footer />
            </Layout>
          </Router>
        </Layout>
      </div>
    );
  };
}

export default App;
