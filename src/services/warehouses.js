import { message } from 'antd';
import { isFunction } from 'lodash';

import Api from 'services/api';
import { getAll } from 'services/base.service';

const WAREHOUSES = 'warehouses';

export const getAllWarehouses = async (search, cb) => {
  const config = {
    errorMessage: 'No se pudo cargar la informacion de los depositos.'
  };
  await getAll(WAREHOUSES, { config }, search, cb)
};

export const getAllWarehousesWithBins = async (search, cb) => {
  const config = {
    errorMessage: 'No se pudo cargar la informacion de los depositos.'
  };
  const bins = { foreign: 'Bin:warehouse:name,description' }
  await getAll(WAREHOUSES, { config }, { ...search, ...bins }, cb)
};

export const getWarehouse = async (id, cb) => {
  const { data } = await Api.getOne(WAREHOUSES, id);
  cb(data);
};

export const createWarehouse = async (record, cb) => {
  try {
    await Api.post(WAREHOUSES, record);
    message.success('Nuevo Almacen fue creado.');

    if (isFunction(cb)) {
      cb();
    }
  } catch (error) {
    console.error(error);
    message.success('Hubo un problema creando el almacen.');
  }
}

export const updateWarehouse = async (record, cb) => {
  try {
    await Api.put(WAREHOUSES, record);
    message.success('Almacen actualizado correctamente.');

    if (isFunction(cb)) {
      cb();
    }
  } catch (error) {
    console.error(error);
    message.success('Hubo un problema actualizando el almacen.');
  }
};

export const deleteWarehouse = async (id, cb) => {
  try {
    await Api.delete(WAREHOUSES, id);
    message.success('Almacen borrado correctamente.');

    if (isFunction(cb)) {
      cb();
    }
  } catch (error) {
    console.error(error);
    message.success('Hubo un problema borrando el almacen.');
  }
};