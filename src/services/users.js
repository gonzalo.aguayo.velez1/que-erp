import { message } from 'antd';
import {isFunction} from 'lodash';

import Api from 'services/api';

const resource = 'users';

export const getAllUsers = async (search, cb) => {
  const {data, meta: {pagination}} = await Api.getAll(resource, search);
  cb({data, pagination});
};

export const createUser = async (record, cb) => {
  try {
    await Api.post(resource, record);
    message.success('Nuevo Usuario fue creado.');

    if (isFunction(cb)) {
      cb();
    }
  } catch (error) {
    console.error(error);
    message.success('Hubo un problema creando el usuario.');
  }
}

export const updateUser = async (record, cb) => {
  try {
    await Api.put(resource, record);
    message.success('Usuario actualizado correctamente.');

    if (isFunction(cb)) {
      cb();
    }
  } catch (error) {
    console.error(error);
    message.success('Hubo un problema actualizando el usuario.');
  }
};

export const deleteUser = async (id, cb) => {
  try {
    await Api.delete(resource, id);
    message.success('Usuario borrado correctamente.');

    if (isFunction(cb)) {
      cb();
    }
  } catch (error) {
    console.error(error);
    message.success('Hubo un problema borrando el usuario.');
  }
};