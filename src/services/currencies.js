import Api from 'services/api';
import { create, getAll, update, deleteOne } from 'services/base.service';

const CURRENCIES = 'currencies';

export const getAllCurrencies = async (cb) => {
  const search = {};
  const config = {
    errorMessage: 'Hubo un problema al cargar las monedas, vuelta a intentarlo.'
  };
  getAll(CURRENCIES, config, search, cb);
};

export const createCurrency = async (record, cb) => {
  const config = {
    succesMessage: 'Moneda fue creada correctamente.',
    errorMessage: 'Hubo un problema al crear la moneda.'
  };

  create(CURRENCIES, config, record, cb);
}

export const updateCurrency = async (record, cb) => {
  const config = {
    succesMessage: 'Moneda fue actualizada correctamente',
    errorMessage: 'Hubo un problema actualizando la Moneda.'
  };

  update(CURRENCIES, config, record, cb);
};

export const deleteCurrency = async (record, cb) => {
  const config = {
    succesMessage: 'Moneda fue borrada correctamente',
    errorMessage: 'Hubo un problema borrando la Moneda.'
  };

  deleteOne(CURRENCIES, config, record, cb);
};