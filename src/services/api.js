import App from 'app.constants';

const responseToJson = response => response.json();
const defaultResponse = ({data, meta}) => ({data, meta});

const API = {
  getOne: (resource, id) => {
    return fetch(`${App.API_URL}/${resource}/${id}`)
      .then(responseToJson);
  },

  getAll: (resource, params={}) => {
    const queryString = Object.keys(params).map(key => `${key}=${params[key]}`).join('&');

    return fetch(`${App.API_URL}/${resource}?${queryString}`)
      .then(responseToJson)
      .then(defaultResponse);
  },

  post: (resource, data) => {
    const request = {
      method: 'POST',
      body: JSON.stringify(data),
      headers:{
        'Content-Type': 'application/json'
      }
    };

    return fetch(`${App.API_URL}/${resource}`, request)
      .then(responseToJson);
  },

  put: (resource, data) => {
    const request = {
      method: 'PUT',
      body: JSON.stringify(data),
      headers:{
        'Content-Type': 'application/json'
      }
    };

    return fetch(`${App.API_URL}/${resource}/${data._id}`, request)
      .then(responseToJson);
  },

  delete: (resource, id) => {
    const request = {
      method: 'DELETE'
    };

    return fetch(`${App.API_URL}/${resource}/${id}`, request)
      .then(responseToJson);
  }
};

export default API;