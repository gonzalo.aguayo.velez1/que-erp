import { message } from 'antd';
import { isFunction } from 'lodash';

import Api from 'services/api';

const resource = 'bins';

export const getAllBins = async (warehouse, cb) => {
  const search = { warehouse, limit: 1000 };
  const { data } = await Api.getAll(resource, search);
  cb(data);
};

export const createBin = async (record, cb) => {
  try {
    await Api.post(resource, record);
    message.success('Nuevo Compartimiento fue creado.');

    if (isFunction(cb)) {
      cb();
    }
  } catch (error) {
    console.error(error);
    message.success('Hubo un problema creando el compartimiento.');
  }
}

export const updateBin = async (record, cb) => {
  try {
    await Api.put(resource, record);
    message.success('Compartimiento actualizado correctamente.');

    if (isFunction(cb)) {
      cb();
    }
  } catch (error) {
    console.error(error);
    message.success('Hubo un problema actualizando el compartimiento.');
  }
};