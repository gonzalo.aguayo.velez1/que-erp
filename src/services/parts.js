import { message } from 'antd';
import { isFunction } from 'lodash';

import Api from 'services/api';
import { create, getAll, getOne, update } from 'services/base.service';

const PARTS = 'parts';
const REVISIONS = 'revisions';

export const getAllParts = async (search, cb) => {
  const { data, meta: { pagination } } = await Api.getAll(PARTS, search);
  cb({ data, pagination });
};

export const getPart = async (id, cb) => {
  const { data } = await Api.getOne(PARTS, id);
  cb(data);
};

export const createPart = async (record, cb) => {
  try {
    await Api.post(PARTS, record);
    message.success('Nueva Parte fue creada.');

    if (isFunction(cb)) {
      cb();
    }
  } catch (error) {
    console.error(error);
    message.success('Hubo un problema creando la parte.');
  }
}

export const updatePart = async (record, cb) => {
  try {
    await Api.put(PARTS, record);
    message.success('Parte actualizada correctamente.');

    if (isFunction(cb)) {
      cb();
    }
  } catch (error) {
    console.error(error);
    message.success('Hubo un problema actualizando la parte.');
  }
};

export const deletePart = async (id, cb) => {
  try {
    await Api.delete(PARTS, id);
    message.success('Parte borrada correctamente.');

    if (isFunction(cb)) {
      cb();
    }
  } catch (error) {
    console.error(error);
    message.success('Hubo un problema borrando la parte.');
  }
};

export const getAllRevisions = async (id, cb) => {
  const search = { part: id, populate: 'bins.bin' };
  const config = {
    errorMessage: 'Hubo un problema al cargar las revisiones, vuelta a intentarlo.'
  };
  getAll(REVISIONS, config, search, cb);
};

export const getRevision = async (id, cb) => {
  const config = {
    errorMessage: 'Hubo un problema cargando la informacion de la Revision.',
  }
  const { data } = await getOne(REVISIONS, config, id, cb);
  cb(data);
};

export const createRevision = async (record, cb) => {
  const config = {
    succesMessage: 'Revision fue creada correctamente',
    errorMessage: 'Hubo un problema al crear la revision'
  };

  create(REVISIONS, config, record, cb);
};

export const updateRevision = async (record, cb) => {
  const config = {
    succesMessage: 'Revision fue actualizada correctamente',
    errorMessage: 'Hubo un problema actualizando la Revision.'
  };

  update(REVISIONS, config, record, cb);
};