import { message } from 'antd';
import { isFunction } from 'lodash';

import Api from 'services/api';

export const create = async (resource, config, record, cb) => {
  const { succesMessage, errorMessage } = config;

  try {
    await Api.post(resource, record);
    message.success(succesMessage);

    if (isFunction(cb)) {
      cb();
    }
  } catch (error) {
    console.error(error);
    message.success(errorMessage);
  }
};

export const update = async (resource, config, record, cb) => {
  const { succesMessage, errorMessage } = config;
  
  try {
    await Api.put(resource, record);
    message.success(succesMessage);

    if (isFunction(cb)) {
      cb();
    }
  } catch (error) {
    console.error(error);
    message.success(errorMessage);
  }
};

export const getOne = async (resource, config, id, cb) => {
  const { errorMessage } = config;

  try {
    const { data } = await Api.getOne(resource, id);
    cb({ data });
  } catch (error) {
    console.error(error);
    message.success(errorMessage);
  }
}

export const getAll = async (resource, config, search, cb) => {
  const { errorMessage } = config;

  try {
    const { data } = await Api.getAll(resource, search);
    cb({ data });
  } catch (error) {
    console.error(error);
    message.success(errorMessage);
  }
}

export const deleteOne = async (resource, config, id, cb) => {
  const { succesMessage, errorMessage } = config;
  
  try {
    await Api.delete(resource, id);
    message.success(succesMessage);

    if (isFunction(cb)) {
      cb();
    }
  } catch (error) {
    console.error(error);
    message.success(errorMessage);
  }
}