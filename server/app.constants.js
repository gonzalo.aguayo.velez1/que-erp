const { ENV } = require('./config.json');

// eslint-disable-next-line import/no-mutable-exports
let appConstants = {};

switch (process.env.NODE_ENV) {
  case 'production': {
    appConstants = ENV.PRODUCTION;
    break;
  }
  case 'development':
  default: {
    appConstants = ENV.DEVELOPMENT;
    break;
  }
}
export default appConstants;
