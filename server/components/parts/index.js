import CRUD from '../../import/api.crud';
import { model } from './parts.model';

export default class Part extends CRUD {
  path = '/parts';

  constructor(router) {
    super(model, 'Part');

    const routes = {
      [this.path]: {
        get: this['all.get'],
        post: this['all.post'],
      },

      [`${this.path}/:id`]: {
        get: this['one.get'],
        put: this['one.put'],
        delete: this['one.delete'],
      },
    };

    super.init(router, routes);
  }
}
