import mongoose, { Schema } from 'mongoose';

const name = 'Part';

const obj = {
  modelCode: String,
  name: String,
  description: String,
  material: String,
  color: String,
  shape: String,
  image: String,
};

const schema = new Schema(obj, { timestamps: true });

export const model = mongoose.model(name, schema);

// db.getCollection('parts').dropIndexes();
// db.getCollection('parts').createIndex({
//   modelCode: 'text',
//   name: 'text',
//   description: 'text',
//   material: 'text',
//   phone: 'text',
//   color: 'text',
// }, {
//     name: 'FullTextSearchIndex',
//     weights: {
//       modelCode: 10,
//       name: 10,
//       description: 8,
//       material: 5,
//       phone: 5,
//       color: 3,
//     }
//   });