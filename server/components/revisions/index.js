import CRUD from '../../import/api.crud';
import { model } from './revisions.model';

export default class Part extends CRUD {
  path = '/revisions';

  constructor(router) {
    super(model, 'Revision');

    const routes = {
      [this.path]: {
        get: this['all.get'],
        post: this['all.post'],
      },

      [`${this.path}/:id`]: {
        get: this['one.get'],
        put: this['one.put'],
        delete: this['one.delete'],
      },
    };

    super.init(router, routes);
  }
}
