import mongoose, { Schema } from 'mongoose';

const name = 'Revision';

const obj = {
  part: {
    type: Schema.Types.ObjectId,
    ref: 'part'
  },
  name: String,
  description: String,
  material: String,
  color: String,
  shape: String,
  image: String,
  unitPrice: Number,
  currency: {
    type: Schema.Types.ObjectId,
    ref: 'Currency'
  },
  bins: [{
    bin: {
      type: Schema.Types.ObjectId,
      ref: 'Bin'
    },
    quantity: {
      type: Number,
      default: 0
    },
  }]
};

const schema = new Schema(obj, { timestamps: true });

export const model = mongoose.model(name, schema);

// db.getCollection('revisions').dropIndexes();
// db.getCollection('revisions').createIndex({
//   modelCode: 'text',
//   name: 'text',
//   description: 'text',
//   material: 'text',
//   phone: 'text',
//   color: 'text',
// }, {
//     name: 'FullTextSearchIndex',
//     weights: {
//       modelCode: 10,
//       name: 10,
//       description: 8,
//       material: 5,
//       phone: 5,
//       color: 3,
//     }
//   });