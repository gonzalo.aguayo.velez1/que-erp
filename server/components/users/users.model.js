import mongoose, { Schema } from 'mongoose';

const name = 'User';

const obj = {
  username: String,
  password: String,
  email: String,
  accessCode: String,
  image: String,
  access: {
    type: String,
    enum: ['Administrador', 'Empleado']
  },
  firstName: String,
  lastName: String,
  phone: String,
  address: String,
};

const schema = new Schema(obj, { timestamps: true });

export const model = mongoose.model(name, schema);

// db.getCollection('users').dropIndexes();
// db.getCollection('users').createIndex({
//   username: 'text',
//   email: 'text',
//   firstName: 'text',
//   lastName: 'text',
//   phone: 'text',
//   address: 'text',
// }, {
//     name: 'FullTextSearchIndex',
//     weights: {
//       username: 10,
//       email: 10,
//       firstName: 10,
//       lastName: 8,
//       phone: 5,
//       address: 5,
//     }
//   });