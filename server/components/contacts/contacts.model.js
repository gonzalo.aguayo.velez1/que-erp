import mongoose, { Schema } from 'mongoose';

const name = 'Contact';

const obj = {
  name: String,
  lastName: String,
  addressLine1: String,
  addressLine2: String,
  phone: String
};

const schema = new Schema(obj, { timestamps: true });

export const model = mongoose.model(name, schema);
