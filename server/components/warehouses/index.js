import CRUD from '../../import/api.crud';
import { model } from './warehouses.model';

export default class Part extends CRUD {
  path = '/warehouses';

  constructor(router) {
    super(model, 'Warehouse');

    const routes = {
      [this.path]: {
        get: this['all.get'],
        post: this['all.post'],
      },

      [`${this.path}/:id`]: {
        get: this['one.get'],
        put: this['one.put'],
        delete: this['one.delete'],
      },
    };

    super.init(router, routes);
  }
}
