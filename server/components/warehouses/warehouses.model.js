import mongoose, { Schema } from 'mongoose';

const name = 'Warehouse';

const obj = {
  name: String,
  description: String,
  enable: {
    type: Boolean,
    default: true
  },
};

const schema = new Schema(obj, { timestamps: true });

export const model = mongoose.model(name, schema);

// db.getCollection('warehouses').dropIndexes();
// db.getCollection('warehouses').createIndex({
//   name: 'text',
//   description: 'text',
// }, {
//     name: 'FullTextSearchIndex',
//     weights: {
//       name: 10,
//       description: 8,
//     }
//   });