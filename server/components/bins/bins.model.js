import mongoose, { Schema } from 'mongoose';

const name = 'Bin';

const obj = {
  name: String,
  description: String,
  warehouse: {
    type: Schema.Types.ObjectId,
    ref: 'Warehouse'
  },
  enable: {
    type: Boolean,
    default: true
  },
};

const schema = new Schema(obj, { timestamps: true });

export const model = mongoose.model(name, schema);

// db.getCollection('bins').dropIndexes();
// db.getCollection('bins').createIndex({
//   name: 'text',
//   description: 'text',
// }, {
//     name: 'FullTextSearchIndex',
//     weights: {
//       name: 10,
//       description: 8,
//     }
//   });