import CRUD from '../../import/api.crud';
import { model } from './clients.model';

export default class Part extends CRUD {
  path = '/clients';

  constructor(router) {
    super(model, 'Client');

    const routes = {
      [this.path]: {
        get: this['all.get'],
        post: this['all.post'],
      },

      [`${this.path}/:id`]: {
        get: this['one.get'],
        put: this['one.put'],
        delete: this['one.delete'],
      },
    };

    super.init(router, routes);
  }
}
