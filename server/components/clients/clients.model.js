import mongoose, { Schema } from 'mongoose';

const name = 'Client';

const obj = {
  numberClient: String,
  name: String,
  fatherSurname: String,
  motherSurname: String,
  addressLine1: String,
  addressLine2: String,
  phone: String,
  cellPhone: String,
};

const schema = new Schema(obj, { timestamps: true });

export const model = mongoose.model(name, schema);
