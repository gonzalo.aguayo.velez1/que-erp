import mongoose from 'mongoose';

import { logger } from './import/logger';
import constants from './app.constants';

const useNewUrlParser = true;
const auth = constants.DB_AUTH.user ? constants.DB_AUTH : null;
const options = { useNewUrlParser, auth };

export const database = {
  init(app) {
    mongoose.connect(
      constants.DB_CONNECTION_STRING,
      options,
    );

    mongoose.connection.on('connected', () => {
      logger.info(`Mongoose default connection open to ${constants.DB_HOST} 🗄`);
    });

    mongoose.connection.on('error', err => {
      logger.error(`Mongoose default connection error: ${err}`);
    });

    mongoose.connection.on('disconnected', () => {
      logger.warning('Mongoose default connection disconnected');
    });

    app.on('SIGINT', () => {
      mongoose.connection.close(() => {
        logger.info('Mongoose default connection disconnected through app termination');
        app.exit(0);
      });
    });
  },
};
