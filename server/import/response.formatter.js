const STATUS_CODES = {
  SUCCESS: 200,
  CREATED: 201,
  NO_CONTENT: 204,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  INTERNAL_SERVER_ERROR: 500,
};

const helpers = {
  success: (res, data, meta, statusCode) => {
    const response = { data, meta, statusCode };
    res.json(response);
  },

  error: (res, description, statusCode, clientCode, additionalInfo) => {    
    res.statusCode = statusCode;
    res.json({
      error: {
        code: clientCode,
        message: description,
        errors: [additionalInfo],
      },
    });
  },
};

export const success = (res, data, meta) => helpers.success(res, data, meta, STATUS_CODES.SUCCESS);

export const created = (res, data) => helpers.success(res, data, undefined, STATUS_CODES.CREATED);

export const noContent = res => helpers.success(res, undefined, undefined, STATUS_CODES.NO_CONTENT);

export const badRequest = (res, message, additionalInfo) =>
  helpers.error(res, message, STATUS_CODES.BAD_REQUEST, STATUS_CODES.BAD_REQUEST, additionalInfo);

export const notFound = (res, message, additionalInfo) =>
  helpers.error(res, message, STATUS_CODES.NOT_FOUND, STATUS_CODES.NOT_FOUND, additionalInfo);

export const forbidden = (res, message, additionalInfo) =>
  helpers.error(res, message, STATUS_CODES.FORBIDDEN, STATUS_CODES.FORBIDDEN, additionalInfo);

export const unauthorized = (res, message, additionalInfo) =>
  helpers.error(res, message, STATUS_CODES.UNAUTHORIZED, STATUS_CODES.UNAUTHORIZED, additionalInfo);

export const internalServerError = (res, message, additionalInfo) =>
  helpers.error(res, message, STATUS_CODES.INTERNAL_SERVER_ERROR, STATUS_CODES.INTERNAL_SERVER_ERROR, additionalInfo);

export const info = (res, message) => {
  res.json({ message });
};

export const raw = (res, data) => {
  res.json(data);
};
