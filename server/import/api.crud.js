import url from 'url';
import querystring from 'querystring';
import { forEach, has, isEmpty, isArray, isString, pick } from 'lodash';

import constants from '../app.constants';
import { logger } from './logger';
import { success, badRequest, created, raw, notFound, internalServerError, info } from './response.formatter';
import { log } from 'util';

const getPagination = req => {
  const currentPage = parseInt(req.query.current, 10) || 1;
  const currentLimit = parseInt(req.query.limit, 10) || 10;
  const page = Math.max(currentPage, 1);
  const limit = Math.min(currentLimit, 10);
  const skip = limit * (page - 1);
  const queryParams = querystring.parse(url.query);
  const protocol = constants.IS_PRODUCTION ? 'https' : 'http';
  const host = req.get('host');
  const baseUrl = `${protocol}://${host}`;
  const extraParams = '';
  const getPrev = (till) => {
    const url = `${baseUrl}?page=${page - 1}&limit=${limit}&till=${till}${extraParams}`;

    return page > 1 ? url : undefined;
  };
  const getNext = (till, totalCount) => {
    const url = `${baseUrl}?page=${page + 1}&limit=${limit}&till=${till}${extraParams}`;
    return totalCount / limit > page ? url : undefined;
  };

  return { page, limit, skip, queryParams, baseUrl, getPrev, getNext };
};

export default class CRUD {
  mongooseDataModel = null;
  friendlyName = '';

  static models = [];

  constructor(model, friendlyName) {
    this.mongooseDataModel = model;
    this.friendlyName = friendlyName;

    CRUD.models[friendlyName] = model;
  }

  // GET api/v1/::model::/?page=1&limit=10
  'all.get' = async (req, res) => {
    const paging = getPagination(req);
    const till = parseInt(req.query.till) || Date.now();
    const $lt = till;
    const filters = { updatedAt: { $lt } };
    let sortParams = { updatedAt: -1 };

    // GEO LOCALIZATION FILTER
    let hasLocation = false;
    const EARTH_RADIUS = 6371;
    const normalizedMaxDistance = (req.query.distance || 10) / EARTH_RADIUS; // 10km default

    this.mongooseDataModel.schema.eachPath(path => {
      if (req.query[path]) {
        // if Array search by regexp CaseInsensitive
        if (this.mongooseDataModel.schema.paths[path].instance === 'Array') {
          filters[path] = { $regex: new RegExp(`^${req.query[path]}$`, 'gi') };
        } else {
          filters[path] = req.query[path];
        }
      }
      hasLocation = path === 'location' ? true : hasLocation;
    });

    if (req.query.longitude && req.query.latitude && hasLocation) {
      filters.location = {
        $near: [parseFloat(req.query.longitude), parseFloat(req.query.latitude)],
        $maxDistance: normalizedMaxDistance,
      };
    }

    // SEARCH
    if (req.query.search) {
      filters.$text = { $search: req.query.search };
      sortParams = { score: { $meta: 'textScore' } };
    }

    // INACTIVE
    if (req.query.inactive) {
      filters.inactive = { $not: { $gt: false } };
    }

    try {
      const total = await this.mongooseDataModel.countDocuments(filters).exec();
      const fields = (req.query.fields || '').replace(/,/g, ' ').replace(/%2/g, ' ');
      let results = await this.mongooseDataModel
        .find(filters, { score: { $meta: 'textScore' } })
        .populate(req.query.populate || '')
        .select(fields)
        .sort(sortParams)
        .skip(paging.skip)
        .limit(paging.limit)
        .exec();

      // FOREIGN
      // warehouses?foreign=Bin:warehouse:name,description
      // Search in a foreign model using the Collection,Currentkey and fields
      if (req.query.foreign) {
        const [collection, fieldKey, fields] = req.query.foreign.split(':');
        const selectedFields = (fields || '').replace(/,/g, ' ').replace(/%2/g, ' ');
        const foreignModel = CRUD.models[collection];

        const addForeignInformation = async entity => {
          if (foreignModel) {
            const relatedResults = await foreignModel
              .find({ [fieldKey]: entity._id })
              .select(selectedFields)
              .exec();

            entity._doc[collection] = relatedResults;
          }

          return entity;
        };
        results = await Promise.all(results.map(addForeignInformation));
      }

      const meta = {
        filters,
        pagination: {
          total,
          limit: paging.limit,
          current: paging.page,
          pages: Math.ceil(total / paging.limit),
          // next: paging.getNext(till, total),
          // prev: paging.getPrev(till),
        },
      };

      success(res, results, meta);
    } catch (err) {
      logger.error(err);
      internalServerError(res, err.message || err.name, err.errors);
    }
  }

  // POST api/v1/::model::
  'all.post' = async (req, res) => {
    try {
      const MongooseDataModel = this.mongooseDataModel;
      const objectSchema = new MongooseDataModel();

      this.mongooseDataModel.schema.eachPath(path => {
        if (['_id', '__v', 'updatedAt', 'createdAt'].indexOf(path) === -1 && req.body[path] !== undefined) {
          objectSchema[path] = req.body[path];
        }
      });

      const newObject = await objectSchema.save();
      created(res, newObject);
    } catch (err) {
      logger.error(err);
      internalServerError(res, err.message || err.name, err.errors);
    }
  }

  // OPTIONS api/v1/::model::
  'all.options' = async (req, res) => {
    const result = {};
    const simplifiedSchema = {};
    const schemaPaths = this.mongooseDataModel.schema.paths;

    Object.keys(schemaPaths)
      .filter(key => has(schemaPaths, key))
      .forEach(key => {
        simplifiedSchema[key] = {};
        simplifiedSchema[key].instance = schemaPaths[key].instance;
        const options = JSON.stringify(schemaPaths[key].options);
        if (options !== '{}') {
          simplifiedSchema[key].options = JSON.parse(options);
        }
      });

    result[this.friendlyName] = simplifiedSchema;
    raw(res, result);
  }

  // GET api/v1/::model::/:id
  'one.get' = async (req, res) => {
    try {
      const fields = (req.query.fields || '').replace(/,/g, ' ');
      const entity = await this.mongooseDataModel.findById(req.params.id, fields);

      if (!isEmpty(entity)) {
        success(res, entity);
      } else {
        notFound(res, `${this.friendlyName} not found`);
      }
    } catch (err) {
      logger.error(err);
      internalServerError(res, err.message || err.name);
    }
  }

  // PUT api/v1/::model::/:id
  'one.put' = async (req, res) => {
    try {
      const fields = req.query.fields || '';
      const entity = await this.mongooseDataModel.findById(req.params.id);

      if (isEmpty(entity)) {
        notFound(res, `${this.friendlyName} not found`);
        return;
      }

      let parseError = false;

      this.mongooseDataModel.schema.eachPath(path => {
        if (path !== '_id' && path !== '__v' && req.body[path] !== undefined) {
          if (
            isArray(entity[path]) &&
            this.mongooseDataModel.schema.paths[path].options &&
            typeof this.mongooseDataModel.schema.paths[path].options.type[0] === 'object'
          ) {
            // [ObjectId]
            if (
              this.mongooseDataModel.schema.paths[path].options.type[0].type &&
              this.mongooseDataModel.schema.paths[path].options.type[0].type.schemaName === 'ObjectId'
            ) {
              entity[path] = req.body[path];
              return;
            }

            // Embedded documents
            let collection = req.body[path];
            if (!Array.isArray(req.body[path])) {
              collection = [req.body[path]];
            }

            entity[path] = collection
              .map(element => {
                let parsed = null;
                try {
                  if (isString(element)) {
                    parsed = JSON.parse(element);
                  } else {
                    parsed = element;
                  }
                } catch (error) {
                  logger.error(error);
                  parseError = true;
                }
                return parsed;
              })
              .filter(element => element !== null);
          } else {
            entity[path] = req.body[path];
          }
        }
      });

      if (parseError) {
        badRequest(res, 'Parse Error');
      }

      await entity.save();

      success(res, fields ? pick(entity, fields.split(',')) : entity);
    } catch (err) {
      logger.error(err);
      internalServerError(res, err.message || err.name);
    }
  }

  // DELETE api/v1/::model::/:id
  'one.delete' = async (req, res) => {
    try {
      const entity = await this.mongooseDataModel.remove({ _id: req.params.id });

      if (entity.n === 0) {
        notFound(res, `${this.friendlyName} not found`);
        return;
      }

      info(res, `${this.friendlyName} successfully deleted`);
    } catch (err) {
      logger.error(err);
      internalServerError(res, err.message || err.name);
    }
  }

  init(router, routes) {
    forEach(routes, (verbs, path) => {
      logger.info(`⚡ Route: ${path} [${Object.keys(verbs)}]`);

      forEach(verbs, (controller, verb) => {
        router.route(path)[verb](controller);
      });
    });
  }
}
