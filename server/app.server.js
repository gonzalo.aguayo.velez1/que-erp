import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { forEach } from 'lodash';

import { logger } from './import/logger';
import { database } from './app.db';
import { internalServerError, notFound } from './import/response.formatter';

import * as Components from './components';

const port = process.env.PORT || 3001;
const host = process.env.HOST || '127.0.0.1';
const app = express();
const router = express.Router();

// CORS
app.use(cors())

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// Database
database.init(app);

// Routes
logger.info('🚦Loading routes:');
forEach(Components, ClassFunction => new ClassFunction(router));
app.use('/api/v1', router);
router.get('/api', (req, res) => {
  const routes = router.stack.filter(obj => obj.route).map(obj => ({
    endpoint: obj.route.path,
    methods: obj.route.methods,
  }));

  res.send(routes);
});

// 404
app.use((req, res) => notFound(res, 'Not Found!'));

// 500
app.use((err, req, res) => {
  logger.error(err);
  internalServerError(res, err.response || 'Oops something went wrong!');
});

// Start App
app.listen(port, host, err => {
  if (err) {
    return logger.error(err.message);
  }

  return logger.appStarted(port, host);
});
